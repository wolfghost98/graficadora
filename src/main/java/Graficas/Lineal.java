/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graficas;

import Files.Columna;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author Carlos
 */
public class Lineal {
       Columna leyenda;
    ArrayList<Columna> datos;

    /**
     * CONTRUCTOR DE LA CLASE
     * @param leyenda
     * @param datos 
     */
    public Lineal(Columna leyenda, ArrayList<Columna> datos) {
        this.leyenda = leyenda;
        this.datos = datos;
    }
    
    
    public void graficar(){
        String title = (leyenda != null) ? leyenda.nombre : " Sin datos";
        JFreeChart chart = ChartFactory.createLineChart(title, "X", "Y", createDataSet(),PlotOrientation.VERTICAL,true,true,false);
        ChartPanel panel = new ChartPanel(chart);
        
        JFrame frame = new JFrame();
        frame.add(panel);
        frame.setLocationRelativeTo(null);
        frame.setSize(800, 400);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }
    
    
    /**
     * Crea la informacion para la grafica de barras
     * @return 
     */
    private CategoryDataset createDataSet(){
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        if(leyenda == null) return dataset;
        for(Columna c : datos){
            for(int i = 0; i < leyenda.datos.size(); i++){
                dataset.setValue(Double.parseDouble(c.datos.get(i)), c.nombre, leyenda.datos.get(i));
            }
        }
        
        return dataset;
    } 
}
