/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graficas;

import Files.Columna;
import java.text.DecimalFormat;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.PieSectionLabelGenerator;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultKeyedValuesDataset;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
/**
 *
 * @author Carlos
 */
public class Pie {
    ArrayList<Columna> datos;

    /**
     * Constructor de la clase
     * @param datos 
     */
    public Pie(ArrayList<Columna> datos) {
        this.datos = datos;
    }
    
    
    public void graficar(){
        PieDataset dataset = createDataSet();
        String title = (this.datos.size() > 0) ? this.datos.get(0).nombre : "Sin datos";
        if(this.datos.size() != 1) title = "Grafica Pie";
        
        JFreeChart chart = ChartFactory.createPieChart(title, dataset,true,true,false);
        
        PieSectionLabelGenerator labelGenerator = new StandardPieSectionLabelGenerator(
        "({2})", new DecimalFormat("0"), new DecimalFormat("0%"));
        ((PiePlot) chart.getPlot()).setLabelGenerator(labelGenerator);
    
        ChartPanel panel = new ChartPanel(chart);
        
        JFrame frame = new JFrame();
        frame.add(panel);
        frame.setLocationRelativeTo(null);
        frame.setSize(800, 400);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setVisible(true);
    }
    
    
    /**
     * Pasamos toda la informacion a un dataset
     * @return 
     */
    private PieDataset createDataSet(){
        DefaultPieDataset dataset = new DefaultPieDataset();
        if(this.datos.size() == 1){
            for(String s: this.datos.get(0).datos) dataset.setValue(s,Double.parseDouble(s));
        }
        else{
            for(Columna c : this.datos) dataset.setValue(c.nombre, Double.parseDouble(c.datos.get(0)));
        }
        return dataset;
    }
    
    
}
