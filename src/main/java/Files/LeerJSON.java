/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Files;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import javax.swing.JOptionPane;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

/**
 *
 * @author Carlos
 */
public class LeerJSON {
    File file;

    /**
     * CONTRUCTOR DE LA CLASE
     * @param file Archivo JSON
     */
    public LeerJSON(File file) {
        this.file = file;
    }
    
    
    /**
     * Metodo que analiza el archivo y retorna 
     * un Arreglo con la informacion
     * @return 
     */
    public ArrayList<Columna> obtenerInformacion(){
        try {
            ArrayList<Columna> informacion = new ArrayList<>();
            Scanner scanner = new Scanner(this.file);
            String texto = "";
            while(scanner.hasNext()) texto += scanner.nextLine() + "\n";
            scanner.close();
            
            
            JSONArray array = (JSONArray)JSONValue.parse(texto);
            for(Object object:array){
                JSONObject jsonObject = (JSONObject)object;
                for(Iterator iterator = jsonObject.keySet().iterator(); iterator.hasNext();){
                    String key = (String)iterator.next();
                    Columna temp = buscarColumna(informacion, key);
                    if(temp == null){
                        temp = new Columna(key);
                        informacion.add(temp);
                    }
                    
                    temp.datos.add(jsonObject.get(key).toString());
                }
            }
            return informacion;
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "No se ha podido abrir el archivo");
            System.err.println("Error obtenerInformacion JSON: " + ex.getMessage());
            return null;
        }
    }
    
    
    /**
     * Busca una columna en la lista de columnas
     * @param informacion
     * @param nombre
     * @return 
     */
    private Columna buscarColumna(ArrayList<Columna> informacion,String nombre){
        for(Columna c : informacion){
            if(c.nombre.equalsIgnoreCase(nombre)) return c;
        }
        return null;
    }
    
}
