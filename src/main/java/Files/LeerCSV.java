/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Files;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Carlos
 */
public class LeerCSV {
    File file;

    /**
     * CONSTRUCTOR DE LA CLASE
     * SE PASA UN ARCHIVO CSV
     * A ANALIZAR
     * @param file 
     */
    public LeerCSV(File file) {
        this.file = file;
    }
    
    
    /**
     * Metodo que analiza el archivo y retorna 
     * un Arreglo con la informacion
     * @return 
     */
    public ArrayList<Columna> obtenerInformacion(){
        try {
            ArrayList<Columna> informacion = new ArrayList<>();
            Scanner scanner = new Scanner(this.file);
            int contador = 0;
            while(scanner.hasNext()){
                int index = 0;
                List<String> temp = parseLista(scanner.nextLine());
                for(String s : temp){
                    if(contador == 0) informacion.add(new Columna(s));
                    else informacion.get(index).datos.add(s);
                    index++;
                }
                contador++;
            }
            scanner.close();
            return informacion;
        } catch (FileNotFoundException ex) {
            JOptionPane.showMessageDialog(null, "No se ha podido abrir el archivo");
            System.err.println("Error obtenerInformacion CSV: " + ex.getMessage());
            return null;
        }
    }
    
    
    
    /**
     * METODO ENCARGADO DE ANALIZAR LA LINEA, 
     * POR SI VIENE "," O " " "
     * ELIMINANDO LAS , NECESARIAS
     * @param linea
     * @return 
     */
    private List<String> parseLista (String linea){
        List<String> result = new ArrayList<>();
        if(linea == null) return result;
        
        StringBuffer valor = new StringBuffer();
        boolean comillas = false;
        boolean dobleComillas = false;
        boolean inicio = false;
        
        char[] chars = linea.toCharArray();
        
        for(char c : chars){
            if(comillas){
                inicio = true;
                if(c == '\"'){
                    comillas = false;
                    dobleComillas = false;
                }else valor.append(c);
            }else{
                if(c == '\"') comillas = true;
                else if( c == ','){
                    result.add(valor.toString());
                    valor = new StringBuffer();
                    inicio = false;
                }
                else if(c == '\r') continue;
                else if(c == '\n') break;
                else valor.append(c);
            }
        }
        result.add(valor.toString());
        return result;
    }
    
    
}
