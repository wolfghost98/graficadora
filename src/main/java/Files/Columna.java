package Files;


import java.util.ArrayList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Carlos
 */
public class Columna {
    public String nombre;
    public ArrayList<String> datos;

    /**
     * Constructor de la clase
     * @param nombre Nombre de la columna
     */
    public Columna(String nombre) {
        this.nombre = nombre;
        this.datos = new ArrayList<>();
    }
    
    
    /**
     * Metodo que agregara los datos de la columna
     * @param info dato a agregar
     */
    public void addData(String info){
        this.datos.add(info);
    }
    
}